using System.Linq;
using System.Collections.Generic;

namespace CountingSort
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int input;
            Console.WriteLine("Digite um número inteiro e positivo");

            if (int.TryParse(Console.ReadLine(), out input))
            {
                if (input < 0)
                {
                    Console.WriteLine("O valor digitado deve ser um número positivo.");
                }
                else
                {
                    foreach(var item in Sort.CountingSort(input))
                    {
                        Console.Write(item);
                    }
                }
            }
            else
                Console.WriteLine("Este software só aceita entradas de números inteiros e positivos");

        }
    }

    class Sort
    {
        public static IEnumerable<int> CountingSort(int input)
        {
            int[] numbers = Array.ConvertAll(input.ToString().ToCharArray(), c => (int)Char.GetNumericValue(c));
            int[] sortedArray = new int[numbers.Length];

            int minVal = numbers.Min();
            int maxVal = numbers.Max();

            int[] counts = new int[maxVal - minVal + 1];

            for (int i = 0; i < numbers.Length; i++)
                counts[numbers[i] - minVal]++;
        
            counts[0]--;

            for (int i = 1; i < counts.Length; i++)
                counts[i] = counts[i] + counts[i - 1];
         
            for (int i = 0; i < numbers.Length; i++)
                sortedArray[counts[numbers[i] - minVal]--] = numbers[i];

            return sortedArray.Reverse();
        }
    }
}